import pyodbc
from datetime import datetime
import pandas as pd
import numpy as np
import re
from collections import defaultdict
from sklearn.preprocessing import LabelEncoder
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.linear_model import LogisticRegression
from datetime import datetime

class SecondaryDataLoader:
    def __init__(self, sales_server=r"sql1205pik\db6", sales_database="DataMart", uid="11", pwd="***",
                 trusted_connection="Yes"):
        self.SALES_SERVER = sales_server
        self.SALES_DATABASE = sales_database
        self.UID = uid
        self.PWD = pwd
        self.trusted_connection = trusted_connection

    def get_sql_config(self):
        return "DRIVER={SQL Server};Server=" + self.SALES_SERVER + ";DATABASE=" + self.SALES_DATABASE + \
               ";UID=" + self.UID + ";PWD=" + self.PWD + "; Trusted_Connection=" + self.trusted_connection

    def load_train(self, cnt=-1):
        sql1 = """select * 
                     from DataMart.dbo.Map_Results_CIAN
                     """

        sq = pyodbc.connect(self.get_sql_config())
        support = pd.read_sql(sql1, sq)
        return support

    def load_heatmap(self, cnt=-1):
        sql1 = """select * 
                     from DataMart.dbo.Map_Results_CIAN_HeatMap
                     """

        sq = pyodbc.connect(self.get_sql_config())
        support = pd.read_sql(sql1, sq)
        return support

    def outload_score(self, df, pred):
        df['Price'] = pred
        sq = pyodbc.connect(self.get_sql_config())
        cursor = sq.cursor()
        df['Flat_CianId'] = df['Flat_CianId'].astype(int)
        df['Date'] = datetime.now().date()
        row = df[['Flat_CianId', 'Price', 'Date']].values.tolist()
        row = list(map(lambda x: (str(int(x[0])), float(x[1]), str(x[2])), row))
        sql = '''insert into DataMart.[dbo].[Map_CIAN_Predict](CianId, Price, CreateDTM)
                  values(?, ?, ?)'''
        cursor.fast_executemany = True
        # for each in tqdm_notebook(row):
        try:
            cursor.executemany(sql, row)
            cursor.commit()
        except pyodbc.Error as strerror:
            print("Error while writing to DB.")
            print(strerror)
        cursor.close()

    def outload_heatmap(self, df, pred):
        df['Price'] = pred
        sq = pyodbc.connect(self.get_sql_config())
        cursor = sq.cursor()
        df['Flat_CianId'] = df['Flat_CianId'].astype(int)
        df['Date'] = datetime.now().date()
        row = df[['Flat_CianId', 'Price', 'Date']].values.tolist()
        row = list(map(lambda x: (str(int(x[0])), float(x[1]), str(x[2])), row))
        sql = '''insert into DataMart.[dbo].[Map_CIAN_Predict_HeatMap](CianId, Price, CreateDTM)
                  values(?, ?, ?)'''
        # cursor.fast_executemany = True
        # for each in tqdm_notebook(row):
        try:
            cursor.executemany(sql, row)
            cursor.commit()
        except pyodbc.Error:
            print("Error while writing to DB.")
            print(each)
        cursor.close()


class PrimaryDataLoader:
    def __init__(self, sales_server=r"sql1205pik\db6", sales_database="DataMart", uid="11", pwd="", fillna='Default'):
        self.SALES_SERVER = sales_server
        self.SALES_DATABASE = sales_database
        self.UID = uid
        self.PWD = pwd
        self.fillna = fillna

    def remove_crap(self, series: pd.Series) -> pd.Series:
        series = series.fillna('нет информации')
        series = series.apply(lambda x: np.nan if len(x) > 5 or len(x) < 1 else x)
        series = series.astype(float, errors='ignore')
        return series

    def __get_sql_config(self, params=None):
        if params is None:
            sales_server = self.SALES_SERVER
            sales_database = self.SALES_DATABASE
            uid = self.UID
            pwd = self.PWD
        else:
            sales_server = params['sales_server']
            sales_database = params['sales_database']
            uid = params['uid']
            pwd = params['pwd']
        return "DRIVER={SQL Server};Server=" + sales_server + ";DATABASE=" + sales_database + \
               ";UID=" + uid + ";PWD=" + pwd + "; Trusted_Connection=Yes"

    def load_train(self, ):
        sql1 = """Select
  gk_id as project, avg_price, class as gk_class,
  developer, power, location, no_decoration, 
  decoration, blank_decoration, lat, lng, date_salestart,
  Klass_obekta,
  Zastrojshchik,
  Arhitektor,
  Kolichestvo_pomeshchenij,
  Ogorozhena_territoriya,
  Ploshchad_zemelnogo_uchastka,
  Vhodnye_gruppy,
  Detskij_sad,
  Shkola,
  Poliklinika,
  FOK,
  Sportivnaya_ploshchadka,
  Avtomojka,
  Kladovye,
  Kolyasochnye,
  Kondicionirovanie,
  Ventliyaciya,
  Lift,
  Sistema_musorotvedeniya,
  Videonablyudenie,
  Podzemnaya_parkovka,
  Dvor_bez_mashin,
  Mashinomest,
  VysotaPotolkov, 
  address,
  gkname,
  sales_pace,
  avg_area,
  disp_area,
  excess_area,
  asym_area,
  bulk_months,
  flat_accommodation,
  apart_accommodation,
  flat_apart_accommodation,
  other_accommodation,
  date_start,
  [GIS_House_production_place_km],
  [GIS_House_business_km],
  [GIS_House_clinic_km],
  [GIS_House_beauty_building_km],
  [GIS_House_pet_clinic_km],
  [GIS_House_holy_place_km],
  [GIS_House_kindergarden_km],
  [GIS_House_minimarket_km],
  [GIS_House_garage_km],
  [GIS_House_fun_place_km],
  [GIS_House_university_km],
  [GIS_House_culture_complex_km],
  [GIS_House_kafe_km],
  [GIS_House_culture_place_km],
  [GIS_House_hypermarker_km],
  [GIS_House_metro_km],
  [GIS_House_supermarket_km],
  [GIS_House_parking_km],
  [GIS_House_school_km],
  [GIS_House_college_km],
  [GIS_district_Id],
  [GIS_Quarter_Dacia_Square_500m],
  [GIS_Quarter_Company_Square_500m],
  [GIS_Quarter_Airline_Square_500m],
  [GIS_Quarter_Support_Square_500m],
  [GIS_Quarter_Green_Square_500m],
  [GIS_Quarter_Garage_Square_500m],
  [GIS_Quarter_Sport_Square_500m],
  [GIS_Quarter_Pedestrian_Square_500m],
  [GIS_Quarter_Living_Square_500m],
  [GIS_Quarter_Administration_Square_500m],
  [GIS_Quarter_Private_Square_500m],
  [GIS_Quarter_RailStation_Square_500m],
  [GIS_Quarter_Flowerbed_Square_500m],
  [GIS_Quarter_Cemetery_Square_500m],
  [GIS_Quarter_Dacia_Square_1000m],
  [GIS_Quarter_Company_Square_1000m],
  [GIS_Quarter_Airline_Square_1000m],
  [GIS_Quarter_Support_Square_1000m],
  [GIS_Quarter_Green_Square_1000m],
  [GIS_Quarter_Garage_Square_1000m],
  [GIS_Quarter_Sport_Square_1000m],
  [GIS_Quarter_Pedestrian_Square_1000m],
  [GIS_Quarter_Living_Square_1000m],
  [GIS_Quarter_Administration_Square_1000m],
  [GIS_Quarter_Private_Square_1000m],
  [GIS_Quarter_RailStation_Square_1000m],
  [GIS_Quarter_Flowerbed_Square_1000m],
  [GIS_Quarter_Cemetery_Square_1000m],
  [GIS_Quarter_Dacia_Square_3000m],
  [GIS_Quarter_Company_Square_3000m],
  [GIS_Quarter_Airline_Square_3000m],
  [GIS_Quarter_Support_Square_3000m],
  [GIS_Quarter_Green_Square_3000m],
  [GIS_Quarter_Garage_Square_3000m],
  [GIS_Quarter_Sport_Square_3000m],
  [GIS_Quarter_Pedestrian_Square_3000m],
  [GIS_Quarter_Living_Square_3000m],
  [GIS_Quarter_Administration_Square_3000m],
  [GIS_Quarter_Private_Square_3000m],
  [GIS_Quarter_RailStation_Square_3000m],
  [GIS_Quarter_Flowerbed_Square_3000m],
  [GIS_Quarter_Cemetery_Square_3000m],
  [GIS_Water_River_Square_500m],
  [GIS_Water_Other_Square_500m],
  [GIS_Water_Lake_Square_500m],
  [GIS_Water_Pond_Square_500m],
  [GIS_Water_River_Square_1000m],
  [GIS_Water_Other_Square_1000m],
  [GIS_Water_Lake_Square_1000m],
  [GIS_Water_Pond_Square_1000m],
  [GIS_Water_River_Square_3000m],
  [GIS_Water_Other_Square_3000m],
  [GIS_Water_Lake_Square_3000m],
  [GIS_Water_Pond_Square_3000m],
  [GIS_Water_River_Square_5000m],
  [GIS_Water_Other_Square_5000m],
  [GIS_Water_Lake_Square_5000m],
  [GIS_Water_Pond_Square_5000m],
  [GIS_kremlin_km],
  [UpdateDTM],
  [GIS_Quarter_Dacia_km],
  [GIS_Quarter_Company_km],
  [GIS_Quarter_Airline_km],
  [GIS_Quarter_Support_km],
  [GIS_Quarter_Green_km],
  [GIS_Quarter_Garage_km],
  [GIS_Quarter_Sport_km],
  [GIS_Quarter_Pedestrian_km],
  [GIS_Quarter_Living_km],
  [GIS_Quarter_Administration_km],
  [GIS_Quarter_Private_km],
  [GIS_Quarter_RailStation_km],
  [GIS_Quarter_Flowerbed_km],
  [GIS_Quarter_Cemetery_km],
  [GIS_Water_River_km],
  [GIS_Water_Other_km],
  [GIS_Water_Lake_km],
  [GIS_Water_Pond_km],
  [GIS_Metro_Line_Id],
  [GIS_Metro_Station_Id],
  [GIS_Metro_Distance_km]
FROM
  (
    select
      p.*,
      row_number() over(partition by p.project_id order by p.month_num desc) as rank
    from [DataMart].[dbo].[comp_project_avg_price] p
  ) price
INNER JOIN [DataMart].[dbo].[comp_project_params] as params ON price.project_id = params.gk_id
left join [DataMart].[dbo].[comp_project_GIS] as g on params.lat = g.CoordLat and params.lng = g.CoordLng
WHERE
  avg_price > 0 and
  dateadd(month, -6, cast(CURRENT_TIMESTAMP as date)) > date_salestart and
  price.rank <= 30
"""
        sq = pyodbc.connect(self.__get_sql_config())
        data_target = pd.read_sql(sql1, sq)
        sq.commit()
        sq.close()

        data_target['gk_class'] = pd.Categorical(data_target['gk_class'], ordered=True, \
                                                 categories=['Low-cost', 'Комфорт', 'Бизнес', 'Элит'])
        data_target['location'] = pd.Categorical(data_target['location'], ordered=True, \
                                                 categories=['Санкт-Петербург', 'Область', 'Новая Москва', 'Москва'])

        data_target['Klass_obekta'] = data_target['Klass_obekta'].fillna('стандарт').apply(
            lambda x: 'стандарт' if 'эконом' in x else x)
        data_target['Klass_obekta'] = pd.Categorical(data_target['Klass_obekta'], ordered=True, \
                                                     categories=['стандарт', 'комфорт', 'бизнес'])

        data_target['Kolichestvo_pomeshchenij'] = self.remove_crap(data_target['Kolichestvo_pomeshchenij'])

        data_target['Ploshchad_zemelnogo_uchastka'] = self.remove_crap(data_target['Ploshchad_zemelnogo_uchastka'])

        data_target['Vhodnye_gruppy'] = data_target['Vhodnye_gruppy'].map({'да': 1, 'нет': 0})
        data_target['Detskij_sad'] = data_target['Detskij_sad'].astype(float)
        data_target['Shkola'] = self.remove_crap(data_target['Shkola']).astype(float)
        data_target['Poliklinika'] = data_target['Poliklinika'].apply(
            lambda x: 0 if isinstance(x, str) and 'нет' in x else x)
        data_target['Poliklinika'] = data_target['Poliklinika'].astype(float)
        data_target['FOK'] = data_target['FOK'].apply(lambda x: 0 if isinstance(x, str) and 'нет' in x else x).astype(
            float)
        data_target['Sportivnaya_ploshchadka'] = data_target['Sportivnaya_ploshchadka'].map({'да': 1})
        data_target['Avtomojka'] = data_target['Avtomojka'].map({'да': 1})
        data_target['Kladovye'] = data_target['Kladovye'].map({'да': 1})
        data_target['Kolyasochnye'] = data_target['Kolyasochnye'].map({'да': 1})
        data_target['Kondicionirovanie'] = self.remove_crap(data_target['Kondicionirovanie'])
        data_target['Ventliyaciya'] = self.remove_crap(data_target['Ventliyaciya']).fillna(0)
        data_target['Lift'] = self.remove_crap(data_target['Lift']).fillna(0)
        data_target['Sistema_musorotvedeniya'] = data_target['Sistema_musorotvedeniya'].astype(str)
        data_target['Videonablyudenie'] = self.remove_crap(data_target['Videonablyudenie'])
        data_target['Podzemnaya_parkovka'] = self.remove_crap(data_target['Podzemnaya_parkovka']).map({"да": 1})
        data_target['Dvor_bez_mashin'] = self.remove_crap(data_target['Dvor_bez_mashin']).map({"да": 1})
        data_target['Mashinomest'] = self.remove_crap(data_target['Mashinomest'])
        data_target['VysotaPotolkov'] = self.remove_crap(data_target['VysotaPotolkov']).fillna('2.75').apply(
            lambda x: re.sub(",", ".", x))
        data_target['VysotaPotolkov'] = data_target['VysotaPotolkov'].apply(lambda x: re.sub("-.*", '', x)).astype(
            float)
        data_target['Klass_obekta'] = pd.Categorical(data_target['Klass_obekta'].fillna('стандарт'), ordered=True, \
                                                     categories=['стандарт', 'комфорт', 'бизнес'])
        data_target['Arhitektor'] = data_target['Arhitektor'].astype(str)
        data_target['Ogorozhena_territoriya'] = data_target['Ogorozhena_territoriya'].astype(str)
        data_target['project'] = data_target['project'].astype(int)

        data_target['date_salestart'] = pd.to_datetime(data_target['date_salestart'])
        data_target = data_target.drop(['developer', 'Zastrojshchik', 'Arhitektor', 'address'], axis=1)
        #         data_target.drop(['GIS_Metro_Station_Id',
        #                           "GIS_Metro_Line_Id",
        #                            "GIS_district_Id"], inplace=True)
        # try to fillna
        #         with open("SecondaryModel\Gis_district_mapping.json", "r") as f:
        #             gis_district_dict = json.load(f)

        #         data_target['GIS_district'] = data_target['GIS_district'].map(gis_district_dict)
        for col in data_target.columns:
            if "Square" in col:
                data_target[col] = data_target[col].fillna(0).astype(float)
            if "_km" in col:
                data_target[col] = data_target[col].fillna(65535).astype(float)
        mlb = MultiLabelEncoder()
        data_target = mlb.transform(data_target)
        #imp = IterativeImputer(estimator=LogisticRegression(),
                               # missing_values=np.nan,
                               # initial_strategy='most_frequent',
                               # n_nearest_features=10,
                               # add_indicator=True, verbose=1)
        #new_values = imp.fit_transform(data_target)
        #data_target = pd.DataFrame(data=new_values, index=data_target.index, columns=data_target.columns, dtype=int)
        data_target = mlb.inverse_transform(data_target)

        data_target['decoration'] = data_target['decoration'].fillna(0)
        data_target['no_decoration'] = data_target['no_decoration'].fillna(0)
        data_target['blank_decoration'] = data_target['blank_decoration'].fillna(0)
        data_target['Kolichestvo_pomeshchenij'] = data_target['Kolichestvo_pomeshchenij'].fillna(
            data_target['Kolichestvo_pomeshchenij'].median())
        data_target['Ploshchad_zemelnogo_uchastka'] = data_target['Ploshchad_zemelnogo_uchastka'] \
            .fillna(data_target['Ploshchad_zemelnogo_uchastka'].median())
        data_target['Vhodnye_gruppy'] = data_target['Vhodnye_gruppy'].fillna(0)
        data_target['Detskij_sad'] = data_target['Detskij_sad'].fillna(0)
        data_target['Shkola'] = data_target['Shkola'].fillna(0)
        data_target['Poliklinika'] = data_target['Poliklinika'].fillna(0)
        data_target['FOK'] = data_target['FOK'].fillna(0)
        data_target['Sportivnaya_ploshchadka'] = data_target['Sportivnaya_ploshchadka'].fillna(0)
        data_target['Avtomojka'] = data_target['Avtomojka'].fillna(0)
        data_target['Kladovye'] = data_target['Kladovye'].fillna(0)
        data_target['Kolyasochnye'] = data_target['Kolyasochnye'].fillna(0)
        data_target['Kondicionirovanie'] = data_target['Kondicionirovanie'].fillna(0)
        data_target['Videonablyudenie'] = data_target['Videonablyudenie'].fillna(0)
        data_target['Podzemnaya_parkovka'] = data_target['Podzemnaya_parkovka'].fillna(0)
        data_target['Dvor_bez_mashin'] = data_target['Dvor_bez_mashin'].fillna(0)
        data_target['Mashinomest'] = data_target['Mashinomest'].fillna((data_target['Mashinomest']).median())

        return data_target

    def load_scores(self, df):
        pass

class MultiLabelEncoder:
    def __init__(self):
        self.d = defaultdict(LabelEncoder)

    def typecheck(self, x):
        try:
            return not isinstance(x.dtype, (object, int))
        except TypeError:
            print(x.dtype)

    def transform(self, df):
        return df.apply(lambda x: x if self.typecheck(x) else self.d[x.name].fit_transform(x))

    def inverse_transform(self, df):
        return df.apply(lambda x: x if self.typecheck(x) else self.d[x.name].inverse_transform(x))


class InferenceDataLoader:
    def __init__(self, sales_server=r"sql1205pik\db6", sales_database="DataMart", uid="11", pwd="***",
                 trusted_connection="Yes"):
        self.SALES_SERVER = sales_server
        self.SALES_DATABASE = sales_database
        self.UID = uid
        self.PWD = pwd
        self.trusted_connection = trusted_connection
        self.outload_columns = []

    def get_sql_config(self):
        return "DRIVER={SQL Server};Server=" + self.SALES_SERVER + ";DATABASE=" + self.SALES_DATABASE + \
               ";UID=" + self.UID + ";PWD=" + self.PWD + "; Trusted_Connection=" + self.trusted_connection

    def get_inpt(self):
        sql0 = "exec DataMart.dbo.UpdatePrimaryToPredict"
        sql1 = """select * from DataMart.dbo.comp_project_to_predict
                     """

        sq = pyodbc.connect(self.get_sql_config())
        cursor = sq.cursor()
        cursor.execute(sql0)
        cursor.commit()
        cursor.close()
        content = pd.read_sql(sql1, sq)
        content['gk_class'] = content['gk_class'].map({'Комфорт': 1,
                                                       'Low-cost': 0,
                                                       'Бизнес': 2,
                                                       'Элит': 3}).astype(float)
        content['date_salestart'] = pd.to_datetime(content['date_salestart'])
        content['first_finish'] = pd.to_datetime(content['first_finish'])

        return content

    def get_schema_outload(self):
        sql1 = """select * 
                     from DataMart.dbo.comp_project_predict
                     """

        sq = pyodbc.connect(self.get_sql_config())
        schema = pd.read_sql(sql1, sq)
        self.outload_columns = schema.columns
        return schema

    def outload_score_prim(self, df):
        df = df.copy(deep=True)
        schema = self.get_schema_outload()
        sq = pyodbc.connect(self.get_sql_config())
        cursor = sq.cursor()
        df['Id'] = df.index
        df['SubmitDTM'] = datetime.now().date().strftime("%Y-%m-%d %H:%M:%S.%f")
        df['date_salestart_y'] = df['date_salestart_y'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
        df['date_salestart_x'] = df['date_salestart_x'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
        df['first_finish_y'] = df['first_finish_y'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
        df['first_finish_x'] = df['first_finish_x'].dt.strftime("%Y-%m-%d %H:%M:%S.%f")
        for col in schema.columns:
            if col not in df.columns:
                df[col] = -1
        row = df[schema.columns].fillna(-1).values.tolist()
        sql = f'''insert into DataMart.[dbo].[comp_project_predict]({",".join(schema)})
                  values({",".join(["?" for i in schema])})'''
        try:
            cursor.executemany(sql, row)
            cursor.commit()

            sql0 = "exec DataMart.dbo.UpdatePrimaryToShow"
            cursor.execute(sql0)
            cursor.commit()
        except pyodbc.Error:
            print("Error while writing to DB.")
            print(each)
        cursor.close()
