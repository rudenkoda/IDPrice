from functools import lru_cache
import lightgbm as lgb
import numpy as np
import pandas as pd
import pyodbc
from Preprocessors import fillna_Gis
from TimeGeoContext import generate_examples_inference
from DataLoaders import InferenceDataLoader
from haversine import haversine_vector
import logging

def get_sql_config():
    return "DRIVER={SQL Server};Server=sql1205pik\db6;DATABASE=DataMart;UID=***;PWD=***; Trusted_Connection=Yes"


def form_query_string(lat, lng):
    query_string = 'POINT(' + str(lat) + ' ' + str(lng) + ')'
    return query_string

def distance_coef(x):
    inverse = 1 / np.power(x, 1)
    return inverse/sum(inverse)

def weighted_sum(data):
    v = distance_coef(haversine_vector(data[['lat_x', 'lng_x']].values, data[['lat_y', 'lng_y']].values))
    data['weight'] = v
    return np.sum(data['weight'] * data['recomended_price_ybyx'])

def get_point(lat, lng):
    sql1 = f"""select * from [DataMart].[dbo].[gis_features_procedure_result]
                    where SourceCoordinatesPoint = '{form_query_string(lat, lng)}'"""
    sq = pyodbc.connect(get_sql_config())
    try:
        points = pd.read_sql(sql1, sq)
    except:
        pass
    sq.close()
    return points


def create_point(lat, lng):
    sql1 = f"""exec [DataMart].[dbo].[get_gis_features] '{form_query_string(lat, lng)}'"""
    print(sql1)
    sq = pyodbc.connect(get_sql_config())

    sq.execute(sql1)
    sq.commit()
    sq.close()


@lru_cache(maxsize=1024)
def get_features_by_coord(lat, lng):
    points = get_point(lat, lng)
    if len(points) < 1:
        create_point(lat, lng)
    points = get_point(lat, lng)
    return points

def get_gis_features(df, model_schema):
    df = df.copy()
    sec_context = pd.concat(list(
    map(lambda x: get_features_by_coord(np.round(x[0], decimals=6), np.round(x[1], decimals=6)),
        zip(df['lng'], df['lat']))))

    for col in sec_context.columns:
        df[col] = sec_context.reset_index(drop=True)[col]

    for col in set(model_schema.columns).difference(set(df.columns)):
        df[col] = model_schema[col].values[0]

    df = fillna_Gis(df)

    df["GIS_district_Id"] = df["GIS_district_Id"].astype(float)
    df["GIS_Metro_Station_Id"] = df["GIS_Metro_Station_Id"].astype(float)
    df["GIS_Metro_Line_Id"] = df["GIS_Metro_Line_Id"].astype(float)
    df['isMoscowPolygon'] = df['isMoscowPolygon'].astype(float).fillna(1)
    return df


def main():
    logger = logging.getLogger("exampleApp")
    logger.info("Getting data for inference")
    loader = InferenceDataLoader()
    inpt = loader.get_inpt()
    logger.info("Idp Loaded")
    heatmap = pd.read_csv('./Data/prim_heatmap.csv', parse_dates=['date_salestart'])
    logger.info("Heatmap loaded")
    cols_to_drop = ['Klass_obekta', 'Kolichestvo_pomeshchenij',
                    'Ogorozhena_territoriya', 'Ploshchad_zemelnogo_uchastka',
                    'Vhodnye_gruppy', 'Detskij_sad', 'Shkola', 'Poliklinika', 'FOK',
                    'Sportivnaya_ploshchadka', 'Avtomojka', 'Kladovye', 'Kolyasochnye',
                    'Kondicionirovanie', 'Ventliyaciya', 'Lift', 'Sistema_musorotvedeniya',
                    'Videonablyudenie', 'Podzemnaya_parkovka', 'Dvor_bez_mashin',
                    'Mashinomest', 'VysotaPotolkov', 'anomaly']
    heatmap = heatmap.drop(columns =cols_to_drop)

    inpt['lat'] = inpt.lat.astype(float)
    inpt['lng'] = inpt.lng.astype(float)
    inpt['project'] = list(range(inpt.shape[0]))
    # schema = pd.read_csv("../modes.csv", index_col=None)
    # schema = pd.DataFrame(data=[schema[schema.columns[1]].values], columns=schema.transpose().iloc[0])

    model_sec = lgb.Booster(model_file='SecondaryModel\sec_model_sec.txt')
    model_schema = pd.read_csv("SecondaryModel\modes.csv", index_col=0)
    logger.info("Secondary model loaded. Getting features by coordinates")
    sec_context = pd.concat(list(
        map(lambda x: get_features_by_coord(np.round(x[0], decimals=6), np.round(x[1], decimals=6)),
            zip(inpt['lng'], inpt['lat']))))

    for col in sec_context.columns:
        inpt[col] = sec_context.reset_index(drop=True)[col]

    for col in set(model_schema.columns).difference(set(inpt.columns)):
        inpt[col] = model_schema[col]

    # for col in model_schema.columns:
    #     inpt[col] = model_schema[col][0]
    inpt = fillna_Gis(inpt)

    inpt["GIS_district_Id"] = inpt["GIS_district_Id"].astype(float)
    inpt["GIS_Metro_Station_Id"] = inpt["GIS_Metro_Station_Id"].astype(float)
    inpt["GIS_Metro_Line_Id"] = inpt["GIS_Metro_Line_Id"].astype(float)
    inpt['isMoscowPolygon'] = inpt['isMoscowPolygon'].astype(float)
    inpt['secondary_price'] = model_sec.predict(inpt[model_schema.columns])
    logger.info("All points secondary price estimated")
    #heatmap = pd.read_csv('../Data/prim_heatmap.csv', parse_dates=['date_salestart'])
    heatmap['bulk_months'] = heatmap['bulk_months'].fillna(heatmap['bulk_months'].median())
    heatmap['first_finish'] = heatmap['bulk_months'].astype('timedelta64[M]') + heatmap['date_salestart'].values
    heatmap.dropna(subset = ['excess_area', 'excess_area'])
    logger.info("Generating pairs to predict")
    ex = generate_examples_inference(heatmap, inpt)
    test = pd.concat(ex)
    test = test.reset_index(drop=True)
    model = lgb.Booster(model_file='prim_model_sec_train_test.txt')
    prim_schema = pd.read_csv("prim_model_sec_schema.csv", index_col=0)
    logger.info("Primary model loaded")
    try:
        test['price_diff_predicted'] = model.predict(test[prim_schema.columns].astype(float))
    except lgb.basic.LightGBMError:
        print("Problem in test data scheme")
        print("Test cols: ", test[prim_schema.columns].columns)
        print("Schema cols: ", prim_schema.columns)
    logger.info("All points predicted")
    def create_submit(train, inpt, heatmap, ):
        train = train.copy()
        train = train.rename(columns={"price_diff_predicted": "recommend_delta_xy",
                                      "non_target_real_price": "real_price_x",
                                      },
                             inplace=False)
    test_submit = test[["gk_class",
                        "power",
                        "location",
                        "no_decoration",
                        "decoration",
                        "blank_decoration",
                        "bulk_months",
                        "secondary_price",
                        'sales_pace',
                        "date_salestart",
                        'flat_accommodation',
                        'apart_accommodation',
                        'flat_apart_accommodation',
                        'other_accommodation',
                        "avg_area",
                        "disp_area",
                        "excess_area",
                        "asym_area",
                        'a_project',
                        'project', 'price_diff_predicted', 'first_finish'
                        ]]

    inpt_submit = inpt[["gk_class",
                        "power",
                        "location",
                        "no_decoration",
                        "decoration",
                        "blank_decoration",
                        "bulk_months",
                        "secondary_price",
                        'sales_pace',
                        "date_salestart",
                        'flat_accommodation',
                        'apart_accommodation',
                        'flat_apart_accommodation',
                        'other_accommodation',
                        "avg_area",
                        "disp_area",
                        "excess_area",
                        "asym_area",
                        "project",
                        "gkname",
                        "lng",
                        "lat",
                        'first_finish'
                        ]]

    #inpt_submit['avg_price'] = inpt_submit['avg_price'].astype(float)
    inpt_submit = inpt_submit.sort_values('date_salestart').drop_duplicates("project", keep='first').reset_index(
        drop=True)
    submit_heatmap = heatmap[["avg_price",
                              "gk_class",
                              "power",
                              "location",
                              "no_decoration",
                              "decoration",
                              "blank_decoration",
                              # "lat" ,
                              # "lng",
                              # "kremlin_dist_l2",
                              # "kremlin_dist_l1"  ,
                              # "metro_dist" ,
                              # "kremlin_log_log_distance",
                              # "RoadDist",
                              "bulk_months",

                              "secondary_price",
                              # 'associated_project',
                              # 'project',
                              'sales_pace',
                              "date_salestart",
                              'flat_accommodation',
                              'apart_accommodation',
                              'flat_apart_accommodation',
                              'other_accommodation',
                              "avg_area",
                              "disp_area",
                              "excess_area",
                              "asym_area",
                              "project",
                              "gkname",
                              "lng",
                              "lat",
                              # "GIS_House_metro_km",
                              # "kremlin_dist_haversine",
                              'first_finish',
                              'date_start'
                              ]].sort_values('date_start', ascending=False).drop_duplicates("project", keep='first').reset_index(
        drop=True)
    submit = pd.merge(inpt_submit,
                      test_submit,
                      left_on='project',
                      right_on='a_project',
                      suffixes=("_y", '_delta'))

    submit = pd.merge(submit,
                      submit_heatmap.add_suffix("_x"),
                      left_on='project_delta',
                      right_on='project_x',
                      suffixes=("", '_x'))

    submit['recomended_price_x'] = submit['avg_price_x'].astype(float) - submit['price_diff_predicted']
    #submit['recomended_price_x_averaged'] = submit.groupby("a_project").recomended_price_x.transform(np.mean)

    submit.rename(columns={"recomended_price_x": "recomended_price_ybyx",
                           "target_real_price": "real_price_y",
                           "non_target_real_price": "real_price_x",
                           # "avg_price_delta" : "recommend_delta_xy",
                           "price_diff_predicted": "recommend_delta_xy",
                           "gkname": "gkname_y",
                           "with_no_decoration_delta": "with_decoration_delta",
                           "recomended_price_x_averaged": "recomended_price_y_averaged",
                           "lat": "lat_y",
                           "lng": "lng_y",
                           'avg_price_x': 'real_price_x',
                           'avg_price_y': 'real_price_y',
                           "a_project": "associated_project",
                           "project_delta": "project"}, inplace=True)
    submit = pd.merge(submit,
                      submit.groupby('associated_project').apply(weighted_sum).rename("recomended_price_y"),
                      left_on='associated_project', right_index=True)

    submit.sort_values("project_y").reset_index(drop=True).to_excel("./primary_compression_submit_test.xlsx",
                                                                    encoding='cp1251')
    submit['gkname_x'] = submit['gkname_x'].astype(str)
    submit['gkname_y'] = submit['gkname_y'].astype(str)
    fillna_dict = {'datetime64[ns]': pd.to_datetime("1970-01-01"), 'float64': -1.0, 'int64': -1, 'object': ''}
    for col in submit.columns:
        try:
            if col != 'recomended_price_x_averaged':
                submit[col] = submit[col].astype(fillna_dict[str(submit[col].dtype)])
        except KeyError:
            print(col)
        except TypeError:
            pass
    logger.info("Submit created")
    d = loader.outload_score_prim(submit)
    logger.info("Submit loaded")


if __name__ == "__main__":
    main()
