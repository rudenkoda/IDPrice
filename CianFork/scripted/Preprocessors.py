#!/usr/bin/python
# -*- coding: utf-8 -*-


from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd

def fillna_Gis(df : pd.DataFrame) -> pd.DataFrame:
    df = df.copy(deep=True)
    for col in df.columns:
        if "Square" in col:
            df[col] = df[col].fillna(0)
        if "_km" in col:
            df[col] = df[col].fillna(65535)
    return df

class PrimaryPreprocessor():
    def __init__(self, features=None):
        self.ifr = IsolationForest(behaviour='new', contamination='auto')
        self.scaler = StandardScaler()
        pass

    def object_filter(self, data):
        return (data.lat < 57) & \
               (data.lng > 35) & \
               (data.lng < 42.5) & (data.gk_class < "Элит") & (data.power > 7)

    def clean(self, data):
        print("\nCleaning data...")
        shape_before = data.shape[0]
        print(f"Got  {shape_before} rows. Unqiue projects count: {data.project.nunique()}")

        time_filtred = data[data.groupby("project").date_salestart.transform(lambda x: np.any(x > "2015-01-01"))]
        price_data = time_filtred[self.object_filter(time_filtred)]
        print(f"{price_data.shape[0]} rows after filtring. Get {price_data.project.nunique()} projects")

        #price_data['avg_price'] = price_data.groupby("project").avg_price.transform(np.median)
        price_data = price_data.drop_duplicates(["project", "date_start"])  # .drop(["sales_pace"], axis=1)
        print(f"Got {price_data.shape[0]} after dropping duplicates")
        return price_data.copy(deep=True)

    def context_clean(self, data):
        print("\nCONTEXT: Cleaning data...")

        shape_before = data.shape[0]
        print(f"Got  {shape_before} rows. Unqiue projects count: {data.project.nunique()}")

        time_filtred = data[data.groupby("project").date_salestart.transform(lambda x: np.any(x > "2012-01-01"))]
        price_data = time_filtred[self.object_filter(time_filtred)]
        print(f"{price_data.shape[0]} rows after filtring. Get {price_data.project.nunique()} projects")

        #price_data['avg_price'] = price_data.groupby("project").avg_price.transform(np.median)
        price_data = price_data.drop_duplicates("project")  # .drop(["sales_pace"], axis=1)
        print(f"Got {price_data.shape[0]} after dropping duplicates")
        return price_data.copy(deep=True)

    def fit_transform(self, data, encode=True, coor_norm=True):
        if encode:
            # excel features
            data['Klass_obekta'] = pd.Categorical(data['Klass_obekta'], ordered=True, \
                                                  categories=['стандарт', 'комфорт', 'бизнес']).codes

            data['Sistema_musorotvedeniya'] = pd.Categorical(data['Sistema_musorotvedeniya']).codes

        if coor_norm:
            transformed_coord = self.scaler.fit_transform(data[['lat', 'lng']])
            data['lat'] = transformed_coord[:, 0]
            data['lng'] = transformed_coord[:, 1]
        if encode:
            data['gk_class'] = pd.Categorical(data['gk_class'], ordered=True, \
                                              categories=['Low-cost', 'Комфорт', 'Бизнес', 'Элит']).codes
            data['location'] = pd.Categorical(data['location'], ordered=True, \
                                              categories=['Санкт-Петербург', 'Область', 'Новая Москва', 'Москва']).codes

        # artificial features
        data['anomaly'] = self.ifr.fit_predict(data[['lat', 'lng', ]])  # 'avg_price']])
        data['power'] = data.power.fillna(data['power'].mean())
        return data

    def get_pace(self, data):
        return data[['date_salestart', 'project', 'sales_pace']].copy(deep=True)

def get_gis_features(df):
    df = df.copy()
    sec_context = pd.concat(list(
        map(lambda x: get_features_by_coord(np.round(x[0], decimals=6), np.round(x[1], decimals=6)),
            zip(df['lng'], df['lat']))))

    for col in sec_context.columns:
        df[col] = sec_context.reset_index(drop=True)[col]

    for col in set(model_schema.columns).difference(set(df.columns)):
        df[col] = model_schema[col].values[0]

    df = fillna_Gis(df)

    df["GIS_district_Id"] = df["GIS_district_Id"].astype(float)
    df["GIS_Metro_Station_Id"] = df["GIS_Metro_Station_Id"].astype(float)
    df["GIS_Metro_Line_Id"] = df["GIS_Metro_Line_Id"].astype(float)
    df['isMoscowPolygon'] = df['isMoscowPolygon'].astype(float).fillna(1)
    return df