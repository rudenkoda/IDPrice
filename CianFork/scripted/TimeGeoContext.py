from tqdm import tqdm

import os
import sys

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsRegressor, DistanceMetric
from pipelineParams import primary_comprassion_cols_to_use
import datetime

class TimeGeoContext(object):
    def __init__(self, time_delta, km_distance):
        self.time_delta = time_delta
        self.km_distance = km_distance
        self.base = None

    def _haversine(self, obj1, obj2):
        dist = DistanceMetric.get_metric('haversine')
        lon1 = np.radians(obj1['lng'])
        lat1 = np.radians(obj1['lat'])
        lon2 = np.radians(obj2['lng'])
        lat2 = np.radians(obj2['lat'])
        km = dist.pairwise([[lat1, lon1], [lat2, lon2]])
        return km[0, 1] * 6378.123708626184

    def haversine_l2(self, row):
        lon2 = np.radians(row['lng'])
        lat2 = np.radians(row['lat'])
        km = self.base.apply(lambda x: self._haversine(row, x), axis=1)
        return km

    def fit(self, base):
        self.base = base.copy(deep=True)
        return self

    def _generate_condidate_context(self, condidate):
        context_neighbours = self.base[(self.base.date_salestart < condidate['date_salestart']) & \
                                       (self.base.date_salestart > pd.to_datetime(
                                           condidate['date_salestart']) - self.time_delta) & \
                                       (self.haversine_l2(condidate) < self.km_distance)].sort_values("date_salestart",
                                                                                                      ascending=False).copy()
        context_neighbours['associated_project'] = condidate['project']
        context_neighbours['abs_day'] = np.abs(
            (pd.to_datetime(context_neighbours['date_salestart']) - pd.to_datetime(condidate['date_salestart'])).dt.days)
        best_match = context_neighbours.sort_values(['project', 'date_start'], ascending=False).drop_duplicates('project',
                                                                                            keep='first').drop(
            columns=['abs_day'])
        return best_match

    def predict(self, condidates):
        ret = {}
        for idx, condidate in condidates.iterrows():
            ret.update({idx: self._generate_condidate_context(condidate)})
        return ret

def generate_examples(df):
    df = df.reset_index(drop=True)
    geo_context_params = {"time_delta": pd.Timedelta(10000, "D"),
                          "km_distance": 7
                          }
    context = TimeGeoContext(**geo_context_params)
    context.fit(df)
    pred_df = df.sort_values('date_salestart', ascending=False).drop_duplicates("project", keep='last').reset_index(drop=True)
    conds = context.predict(pred_df)
    cols_to_use = primary_comprassion_cols_to_use
    for idx, condidate in tqdm(pred_df.iterrows(), total=pred_df.shape[0]):
        # try:
        prices = conds[idx]['avg_price']
        conds[idx] = conds[idx][cols_to_use] - condidate[cols_to_use]
        conds[idx]['target_real_price'] = condidate['avg_price']
        conds[idx]['non_target_real_price'] = prices

        if "date_salestart" in cols_to_use and conds[idx].shape[0] != 0:
            conds[idx]['date_salestart'] = (conds[idx]['date_salestart']).dt.days

        if "first_finish" in cols_to_use and conds[idx].shape[0] != 0:
            conds[idx]['first_finish'] = (conds[idx]['first_finish']).dt.days

    #         except:
    #             print(idx)
    #             print("Conds length", len(conds))
    #             print("problem")
    return conds


from tqdm import tqdm


def generate_examples_inference(df, inpt):
    inpt = inpt.copy(deep=True)
    cols_to_use = list(set(primary_comprassion_cols_to_use).difference({'avg_price'}))
    inpt = inpt.drop_duplicates(('lat', 'lng'))
    inpt['power'] = inpt['power'].fillna(inpt['power'].median())
    inpt['no_decoration'] = inpt['no_decoration'].fillna(0)
    inpt['flat_accommodation'] = inpt['flat_accommodation'].fillna(0)
    inpt['sales_pace'] = inpt['power'] / 5

    df = pd.concat([df, inpt], axis=0)
    df = df.reset_index(drop=True)
    geo_context_params = {"time_delta": pd.Timedelta(10000, "D"),
                          "km_distance": 7
                          }
    context = TimeGeoContext(**geo_context_params)
    context.fit(df)

    pred_df = inpt.sort_values('date_salestart', ascending=False).drop_duplicates("project", keep='first').reset_index(drop=True)
    print("Ascending False")
    if "date_salestart" in cols_to_use:
        pred_df.date_salestart = pred_df.date_salestart.fillna(pd.to_datetime(datetime.datetime.now().date()) + pd.Timedelta('180 days'))
    if "first_finish" in cols_to_use:
        pred_df.loc[pred_df.first_finish.isna(), "first_finish"] = pred_df.loc[pred_df.first_finish.isna(), "date_salestart"] + pd.Timedelta('730 days')

    conds = context.predict(pred_df)
    for idx, condidate in tqdm(pred_df.iterrows(), total=pred_df.shape[0]):
        try:
            prices = conds[idx]['avg_price']
            a_project = condidate['project']
            project = conds[idx]['project']
            date_start = conds[idx]['date_start']
            conds[idx] = conds[idx][cols_to_use] - condidate[cols_to_use]
            #conds[idx]['target_real_price'] = condidate['avg_price']
            conds[idx]['non_target_real_price'] = prices
            conds[idx]['a_project'] = a_project
            conds[idx]['project'] = project
            conds[idx]['date_start'] = date_start
            if "date_salestart" in cols_to_use and conds[idx].shape[0] != 0:
                conds[idx]['date_salestart'] = (conds[idx]['date_salestart']).dt.days
            if "first_finish" in cols_to_use and conds[idx].shape[0] != 0:
                conds[idx]['first_finish'] = (conds[idx]['first_finish']).dt.days
        except KeyError:
            print(conds[idx][cols_to_use])
            print(condidate[cols_to_use])
            print("Conds length", len(conds))
            print("problem")
    return conds