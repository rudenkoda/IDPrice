USELESS_COLUMNS = ['Flat_source', 'Flat_adress', 'Flat_square_rooms',
                   'house_num', 'Flat_comment', 'sale_period',
          'Flat_house_type', 'floor_finish', 'Flat_sale_type',
         'ipoteka', 'LanLatGeography', 'CoordText', 'CoordYandexText',
         'Flat_date1', 'Flat_create_date', "CoordBuffer500"
      ,'CoordBuffer1000'
      ,'CoordBuffer3000'
      ,'CoordBuffer5000', 'UpdateDTM', 'GIS_Metro_Line',
  'GIS_Metro_Station', 'GIS_district_Id',
     'Flat_CoordLng', 'Flat_CoordLat']

best_params_secondary_model = {
     'subsample_for_bin': 200000,
     'subsample': 0.5,
     'reg_lambda': 1,
     'reg_alpha': 1,
     'random_state': 4545,
     'objective': None,
     'num_leaves': 63,
     'n_jobs': -1,
     'n_estimators': 1500,
     'min_split_gain': 0.01,
     'min_child_weight': 0.01,
     'min_child_samples': 3,
     'importance_type': 'split',
     'boosting_type': 'dart',
}

best_params_primary = {
 'subsample_for_bin': 200000,
 'subsample': 0.2,
 'reg_lambda': 1,
 'reg_alpha': 1,
 'random_state': 4545,
 'objective': None,
 'num_leaves': 63,
 'n_jobs': -1,
 'n_estimators': 1000,
 'min_split_gain': 0.01,
 'min_child_weight': 0.01,
 'min_child_samples': 5,
 'max_depth': 5,
 'learning_rate': 0.5,
 'importance_type': 'split',
 'boosting_type': 'dart',
}

primary_comprassion_cols_to_use = [
        "avg_price",
        "gk_class",
        "power",
        "location",
        "no_decoration",
        "decoration",
        "blank_decoration",
        "bulk_months",
        "secondary_price",
        'sales_pace',
        "date_salestart",
        'flat_accommodation',
        'apart_accommodation',
        'flat_apart_accommodation',
        'other_accommodation',
        "avg_area",
        "disp_area",
        "excess_area",
        "asym_area",
        "first_finish"
    ]
